#!/bin/bash

/usr/bin/amixer set Master 95%

if [ $# -eq 0 ]
then
    /usr/bin/play /home/sysadmin/AskLeo2.aiff
else
    /usr/bin/play /home/sysadmin/${1}
fi

