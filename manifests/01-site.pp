Exec {path => "/opt/puppetlabs/bin:/usr/bin:/usr/sbin:/bin:/sbin"}

#Global varials, start with kcs_

$kcs_files = "https://www.kentoncityschools.org/media/puppet"

# 6th grade bells
node /^es-(d9|d10|d11|d12|d13|d14)-bell-*/ {

  include defaults

  # Schedule file
  file { '/var/spool/cron/crontabs/sysadmin':
    ensure => present,
    owner  => "sysadmin",
    group  => "crontab",
    source => "/etc/puppet/files/6thgradecron",
    mode   => "0600",
  }

}

# MS bells
node /^ms-office-bell-*/ {

  include defaults

  # Schedule file
  file { '/var/spool/cron/crontabs/sysadmin':
    ensure => present,
    owner  => "sysadmin",
    group  => "crontab",
    source => "/etc/puppet/files/mscron",
    mode   => "0600",
  }

}
