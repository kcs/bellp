class defaults {
  # Default packages
  $pkgs = [ "alsa-utils","sox","libsox-fmt-all" ]

  package { $pkgs:
    ensure => latest,
  }

  # Keep wifi up
  file { '/root/checkwanup':
    source => "/etc/puppet/files/checkwanup",
    mode   => "0755",
    owner  => "root",
    group  => "root",
    ensure => "present",
  }

  # Make sure sound works
  file { '/etc/modprobe.d/alsa-base.conf':
    ensure => "present",
    source => "/etc/puppet/files/alsa-base.conf",
    mode   => "0644",
    owner  => "root",
    group  => "root",
  }
  
  # Copy the root cron
  file { '/var/spool/cron/crontabs/root':
    ensure => "present",
    source => "/etc/puppet/files/rootcron",
    mode   => "0600",
    owner  => "root",
    group  => "crontab",
  }

  # Copy ring script
  file { '/home/sysadmin/ring.sh':
    ensure => "present",
    source => "/etc/puppet/files/ring.sh",
    mode   => "0755",
    owner  => "sysadmin",
    group  => "sysadmin",
  }

  # Copy sounds
  file { '/home/sysadmin/AskLeo2.aiff':
    ensure => "present",
    source => "$::kcs_files/files/bell/AskLeo2.aiff",
    mode   => "0755",
    owner  => "sysadmin",
    group  => "sysadmin",
  }
  file { '/home/sysadmin/pacman_dying.mp3':
    ensure => "present",
    source => "$::kcs_files/files/bell/pacman_dying.mp3",
    mode   => "0755",
    owner  => "sysadmin",
    group  => "sysadmin",
  }

  file { '/home/sysadmin/alert04.aiff':
    ensure => "present",
    source => "$::kcs_files/files/bell/alert04.aiff",
    mode   => "0755",
    owner  => "sysadmin",
    group  => "sysadmin",
  }

}
